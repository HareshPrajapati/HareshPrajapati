

- 👋 Hi, I’m @HareshPrajapati
- 👀 I’m interested in Embedded firmware development
- 🌱 I’m currently working in  embedded c / c++ and othe IoT and electronics fundamentals
- 💞️ I’m looking to collaborate on embedded IoT solutions and projects(if possible)
- 📫 you can contact me on sondagarharesh@gmail.com or call or whatsapp on +91 7820017492

### Languages and Tools:
<img align="left" alt="C" width="26px" src="https://raw.githubusercontent.com/github/explore/f3e22f0dca2be955676bc70d6214b95b13354ee8/topics/c/c.png" />
<img align="left" alt="C plus plus" width="26px" src="https://raw.githubusercontent.com/github/explore/180320cffc25f4ed1bbdfd33d4db3a66eeeeb358/topics/cpp/cpp.png" />
<img align="left" alt="Python" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png" />
<img align="left" alt="Visual Studio Code" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png" />
<img align="left" alt="HTML5" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/html/html.png" />
<img align="left" alt="CSS3" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/css/css.png" />
<img align="left" alt="JavaScript" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png"/>
<br/><br/>


<table style="width:500px;">
    <tr>
    <th> Language/Script </th>
    <th> Experience Level</th>
  </tr>
  <tr>
    <td> C </td>
    <td>&#11088;&#11088;&#11088;&#11088;&#11088;</td>
  </tr>
  <tr>
    <td> C++ </td>
    <td>&#11088;&#11088;&#11088;</td>
  </tr>
  <tr>
    <td> Assembly </td>
    <td>&#11088;&#11088;</td>
  </tr>
  <tr>
    <td> Javascript </td>
    <td>&#11088;&#11088;&#11088;</td>
  </tr>
  <tr>
    <td> HTML </td>
    <td>&#11088;&#11088;&#11088;</td>
  </tr>
  <tr>
    <td> Python </td>
    <td>&#11088;&#11088;</td>
  </tr>
  <tr>
    <td> Linker File </td>
    <td>&#11088;&#11088;</td>
  </tr>
  <tr>
    <td> Makefile </td>
    <td>&#11088;</td>
  </tr>
</table>

<h3> Used controllers and MCUs:</h3>
<table style="width:fit-content;">
 <tr>
    <th> Provider </th>
    <th> MCU/Boards </th>
  </tr>
  <tr>
    <td> Microchip </td>
    <td> PIC32, PIC16,  PIC24, dsPIC33, PIC18 </td>
  </tr>
  <tr>
    <td> Atmel </td>
    <td>Arduino Uno, Arduino Mega, ATtiny</td>
  </tr>
  <tr>
    <td> Espressif </td>
    <td>   ESP8266, ESP32, ESP32-CAM </td>
  </tr>
  <tr>
    <td> STMicroelectronics </td>
    <td> STM32 </td>
  </tr>
</table>

<h3>Projects:</h3>
<table style="width:fit-content;">
 <tr>
    <th> Project </th>
    <th> Used </th>
    <th> Info </th>
  </tr>
  <tr>
    <td> Security system using firebase </td>
    <td> Firebase  + PIC32 Ethernet starter kit II </td>
    <td> Security system using firebase as database </td>
  </tr>
  <tr>
    <td> IoT device (Scheduler) </td>
    <td> ESP8266 + RTC </td>
    <td> Perform specific event on timer/alarm </td>
  </tr>
  <tr>
    <td> PICxx Bootloaders </td>
    <td> PIC MCUs </td>
    <td> Implemented/modified bootloaders for PICxx controllers with custom linker and verious interface (SD Card, USB, UART, CAN bootloader), Modified EZBL bootloaders </td>
  </tr>
  <tr>
    <td> STM Bootloaders </td>
    <td> STM MCUs </td>
    <td> Implemented/modified bootloaders for STMxx controllers with custom linker and verious interface (SPI Flash, USB, UART bootloader, Bluetooth) </td>
  </tr>
</table>


[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=HareshPrajapati&layout=compact)](https://github.com/anuraghazra/github-readme-stats)

[![HareshPrajapati's GitHub stats](https://github-readme-stats.vercel.app/api?username=HareshPrajapati&show_icons=true&theme=radical)
](https://github.com/anuraghazra/github-readme-stats)

<p ><img  src="https://github-readme-streak-stats.herokuapp.com/?user=HareshPrajapati&theme=dark" width="494" height="195"  alt="aajay5" /></p>


![visitors](https://visitor-badge.glitch.me/badge?page_id=HareshPrajapati.visitor-badge)
[![Twitter Badge](https://img.shields.io/badge/Twitter-Profile-informational?style=flat&logo=twitter&logoColor=white&color=1CA2F1)](https://twitter.com/harrysondagar)
[![LinkedIn Badge](https://img.shields.io/badge/LinkedIn-Profile-informational?style=flat&logo=linkedin&logoColor=white&color=0D76A8)](https://www.linkedin.com/in/haresh-prajapati-70b690179/)

<!---
HareshPrajapati/HareshPrajapati is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->


